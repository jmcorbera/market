package market;

import asset.Asset;
import operation.OperationType;

public interface Market {
	public double getPrice(String symbol);
	public String getCode();
	public boolean placeOrder(double quantity, double marketPrice, Asset asset, OperationType operationType);
	public double getBalance();
}
